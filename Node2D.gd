extends Node2D

var Representer=load("res://representer.gd")
var Link=load("res://Link.gd")
var LinkGroup=load("res://LinkGroup.gd")
var nodes=Array()
var links=Array()
var nets=Array()
var linkgroups=Array()


# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	for C in get_children():
		if C.is_class("Node2D"):
			print("found one!")
			nodes.push_front(C)
	print(nodes.size())
	update_links()
	for L in linkgroups:
		add_child(L)


func update_links():
	var nodarr=Array()
	for C in nodes:
		for N in C.nets:
			if !nets.has(N):
				nets.push_front(N)
	for N in nets:
		for C in nodes:
			if C.nets.has(N):
				nodarr.push_front(C)
		for i in range(nodarr.size()-1):
			for k in range(i+1,nodarr.size()):
				links.push_back(Link.new(nodarr[i],nodarr[k],N))
		nodarr.clear()
	var tlin=links.duplicate()
	while tlin.size()!=0:
		var linn=Array()
		var tlin2=Array()
		for L in tlin:
			if (L.node1==tlin[0].node1)&&(L.node2==tlin[0].node2):
				linn.append(L)
			else:
				tlin2.append(L)
		linkgroups.append(LinkGroup.new(tlin[0].node1,tlin[0].node2,linn))
		tlin=tlin2
#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
