extends Node2D
var node1
var node2
var links = Array()
var color = ColorN("midnightblue")
var highColor = ColorN("aquamarine")
var criticalColor=ColorN("orange")
var criticalHigh=ColorN("pink")
var line
var critical
var basethick=2
var popup
var highlighted=false
const drad=30
const popup_delta=Vector2(10,10)

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	
	line=Line2D.new()
	line.default_color=color
	line.width=basethick*links.size()
	line.z_index=-1
	update_render()
	add_child(line)
	
		
func update_render():
	var pool=Array()
	pool.push_front(node1.position)
	pool.push_front(node2.position)
	line.points=pool

func _init(Node1,Node2,Links):
	node1=Node1
	node2=Node2
	Node1.linkgroups.append(self)
	Node2.linkgroups.append(self)
	links=Links


func _process(delta):
	var d = node2.position-node1.position
	var m=get_global_mouse_position()
	if(is_near(d,m-node1.position)):
		if highlighted==false:
			popup=PopupMenu.new()
			popup.rect_position=m+popup_delta
			for L in links:
				popup.add_item(L.net)
			add_child(popup)
			popup.visible=true
			line.default_color=highColor
			highlighted=true
		else:
			popup.rect_position=m+popup_delta
	else:
		if highlighted==true:
			popup.free()
			line.default_color=color
			highlighted=false 

#	pass

func is_near(vec1,vec2):
	
	var along=vec2.dot(vec1.normalized())
	if along<node1.radius||along>(vec1.length()-node2.radius):
		return false
	if abs(vec2.dot(vec1.tangent().normalized()))<drad:
		return true
	return false 