extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_Button_button_down():
	var fd = FileDialog.new()
	fd.mode=FileDialog.MODE_OPEN_FILES
	fd.access=FileDialog.ACCESS_FILESYSTEM
	fd.add_filter("*.sch")
	fd.connect("files_selected",self,"_onSelected")
	add_child(fd)
	var rec=get_viewport().get_visible_rect().grow(-40)
	fd.rect_position=rec.position
	fd.rect_size=rec.size
	fd.visible=true
	fd.invalidate()
	
	
	
	
func _onSelected(paths):
	print("files selected")
	print(paths.size())
	
