extends Node2D
export var NodeName="name"
var anglestep=6.28/30
var Body
var hovershade=0.5
var radius=30
var nodeColor=ColorN("cyan")
var hovercolor=ColorN("white")
var textureLayer
var selectionCircle
var selectionRadius=32
var Selected=false
var ignoreUnselection=false
var dragging=false
var locked=false
var linkgroups=Array()
export var nets = Array(["net1"])

func _ready():
	selectionCircle=create_polygon(-2,selectionRadius,0,2*PI,ColorN("cyan"))
	add_child(selectionCircle)
	selectionCircle.visible=false
	Body=create_polygon(0,radius)
	add_child(Body)
	selectionCircle
	textureLayer=TextureButton.new()
	add_child(textureLayer)
	textureLayer.connect("mouse_entered",self,"onMouseEnter")
	textureLayer.connect("mouse_exited",self,"onMouseExit")
	textureLayer.connect("button_down",self,"onButtonDown")
	textureLayer.connect("button_up",self,"onButtonUp")
	textureLayer.rect_position=Vector2(radius*-1,radius*-1)
	textureLayer.rect_size=Vector2(radius*2,radius*2)

func _process(delta):
	if dragging:
		position=get_global_mouse_position()
		for L in linkgroups:
			L.update_render()

func onButtonDown():
	Selected=true
	selectionCircle.visible=true
	ignoreUnselection=true
	dragging=true

func onButtonUp():
	Selected=false
	selectionCircle.visible=false
	ignoreUnselection=false
	dragging=false

func onMouseEnter():
	Body.color=hovercolor

func onMouseExit():
	Body.color=nodeColor

func create_polygon(z,rad=100,startang=0,finishang=PI*2,col=ColorN("cyan",1)):
	
	if startang==finishang:
		return
	#centerpoint
	var varang=startang
	var pool=PoolVector2Array()
	pool.push_back(Vector2(0,0))
	#arch
	while (varang<=finishang):
		pool.push_back(rad*Vector2(cos(varang),sin(varang)))
		varang=varang+anglestep
	varang=finishang
	pool.push_back(rad*Vector2(cos(varang),sin(varang)))
	var Poly=Polygon2D.new()
	Poly.color=col
	Poly.polygon=pool
	var outline=Line2D.new()
	pool.remove(0)
	outline.points=pool
	outline.default_color=Poly.color.lightened(0.5)
	outline.width=1.5
	Poly.z_index=z
	outline.z_index=z
	Poly.add_child(outline)
	return Poly
	
#func _init(NodeName,nets):
#	pass