# EEPlanner
A tool to wrap your head around complex stuff pcb layout

## Current status

**PROTOTYPE IS UNDER CONSTRUCTION**

* basic file selection with sch filtering-[YES]
* draggable node (circle) - [YES]
* net fields - [YES]
* line rendering [CURRENT]
  * initial rendering [YES]
  * updates (via drag)[YES]
* highlightable lines [YES]
* netlist[CURRENT]
* sch loading
